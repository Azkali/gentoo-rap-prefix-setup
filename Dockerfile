FROM arm64v8/ubuntu as base

ARG DEBIAN_FRONTEND=noninteractive
ARG DEVICE
ARG USERNAME
ARG USER_ID
ARG EPREFIX

ENV USER_ID=${USER_ID:-"1000"}
ENV USERNAME=${USERNAME:-"user"}
ENV EPREFIX=${EPREFIX:-"/data/gentoo64"}
ENV DEVICE=${DEVICE}

ENV GENTOO_MIRRORS="http://gentoo.mirrors.ovh.net/gentoo-distfiles/"

# Depends on ${EPREFIX}
ENV PATH="${EPREFIX}/usr/bin:${EPREFIX}/bin:${EPREFIX}/tmp/usr/bin:${EPREFIX}/tmp/bin:${PATH}"
ENV IN_PREFIX_RUN="${EPREFIX}/usr/bin/env -i HOME=$HOME TERM=$TERM USER=$USER SHELL=$SHELL ${EPREFIX}/bin/bash --rcfile ${EPREFIX}/etc/prefix-stack.bash_login -i -c "

ENV CI_PROJECT_DIR=${CI_PROJECT_DIR:-{"/home/${USERNAME}/"}}

# Install needed dependencies
RUN apt-get update  -y \
	&& apt-get install -y \
		apt-utils \
		bash-completion \
		net-tools \
		vim \
		python3-pip \
		python3-dev \
		wget \
		libgmp-dev \
		libgmp3-dev \
		python3 \
		python-is-python3 \
		sudo \
		gcc

# Create user
RUN useradd -G sudo -m -s /bin/bash -u ${USER_ID} ${USERNAME}
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}

# Create user accessible termux prefix dir
RUN mkdir -p ${EPREFIX}
RUN chmod ug+w ${EPREFIX}
RUN chown -R ${USERNAME}:${USERNAME} ${EPREFIX}

# Add local bootstrap-prefix script
RUN wget https://gitweb.gentoo.org/repo/proj/prefix.git/plain/scripts/bootstrap-prefix.sh -O /home/${USERNAME}/bootstrap-prefix.sh
RUN chmod +x /home/${USERNAME}/bootstrap-prefix.sh
RUN chown ${USERNAME}:${USERNAME} /home/${USERNAME}/bootstrap-prefix.sh

WORKDIR /home/${USERNAME}
USER ${USERNAME}

ADD assets /home/${USERNAME}/assets
ADD scripts /home/${USERNAME}/scripts

# Bootstrap generic gentoo prefix
CMD ./scripts/build
